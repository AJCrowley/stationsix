<?php
/*
  Plugin Name: Cherry Bgslider Plugin
  Version: 1.0
  Plugin URI: http://www.cherryframework.com/
  Description: Create full bg slider
  Author: Cherry Team.
  Author URI: http://www.cherryframework.com/
  Text Domain: cherry-bgslider
  Domain Path: languages/
  License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
if ( ! defined( 'ABSPATH' ) )
exit;

class cherry_bgslider {
  
  public $version = '1.0';

  function __construct() {
    add_action( 'wp_enqueue_scripts', array( $this, 'assets' ) );
    add_shortcode( 'cherry_bgslider', array( $this, 'bgSlider_shortcode' ) );
    add_action('wp_footer', array( $this, 'bgSliderBuilder' ));
  }

  function assets() {
    if ( is_front_page() ) {
      wp_enqueue_script( 'chrome-smoothing-scroll', $this->url('js/smoothing-scroll.js'), array('jquery'), '1.0', true );
      wp_enqueue_script( 'cherryfullBgSlider', $this->url('js/cherryfullBgSlider.js'), array('jquery'), $this->version, true );
      wp_enqueue_style( 'cherry-bgslider-styles', $this->url('css/cherry-bgslider-styles.css'), '', $this->version );
    }
  }

  /**
   * return plugin url
   */
  function url( $path = null ) {
    $base_url = untrailingslashit( plugin_dir_url( __FILE__ ) );
    if ( !$path ) {
      return $base_url;
    } else {
      return esc_url( $base_url . '/' . $path );
    }
  }

  /**
   * return plugin dir
   */
  function dir( $path = null ) {
    $base_dir = untrailingslashit( plugin_dir_path( __FILE__ ) );
    if ( !$path ) {
      return $base_dir;
    } else {
      return esc_url( $base_dir . '/' . $path );
    }
  }

    /**
     * Shortcode
     */
    function bgSlider_shortcode( $atts, $content = null ) {
          extract(shortcode_atts(array(
          'num'              => '5',
          'type'             => 'slider',
          'effect'           => 'slide',
          'category'         => '',
          'custom_category'  => '',
          'pagination'       => 'true',
          'navigation'       => 'true',
          'custom_class'     => ''
      ), $atts));

      $type_post         = $type;
      $slider_pagination = $pagination;
      $slider_navigation = $navigation;
      $random            = gener_random(10);
      $i                 = 0;
      $rand              = rand();
      $count             = 0;
      if ( is_rtl() ) {
        $is_rtl = 'true';
      } else {
        $is_rtl = 'false';
      }

      $output = '<script type="text/javascript">
              jQuery(window).load(function() {
                jQuery("#flexslider_'.$random.'").flexslider({
                  animation: "'.$effect.'",
                  smoothHeight : true,
                  directionNav: '.$slider_navigation.',
                  controlNav: '.$slider_pagination.',
                  rtl: '.$is_rtl.'
                });
                        
                var extendBgSlider = jQuery(".extendBgSlider");
                jQuery(".flex-direction-nav .flex-prev", extendBgSlider).on("click", function(){
                  jQuery(".full-bg-slider").trigger("switchPrev");
                })
                jQuery(".flex-direction-nav .flex-next", extendBgSlider).on("click", function(){
                  jQuery(".full-bg-slider").trigger("switchNext");
                })
                jQuery(".flex-control-nav > li", extendBgSlider).on("click", function(){
                    jQuery(".full-bg-slider").trigger("switchPrev", jQuery(this).index());
                })
              });    
      ';
      $output .= '</script>';
      $output .= '<div id="flexslider_'.$random.'" class="flexslider extendBgSlider no-bg '.$custom_class.'">';
        $output .= '<ul class="slides">';

        global $post;
        global $my_string_limit_words;

        // WPML filter
        $suppress_filters = get_option('suppress_filters');

        $args = array(
          'post_type'              => $type_post,
          'category_name'          => $category,
          $type_post . '_category' => $custom_category,
          'numberposts'            => $num,
          'orderby'                => 'post_date',
          'order'                  => 'DESC',
          'suppress_filters'       => $suppress_filters
        );

        $latest = get_posts($args);

        foreach($latest as $key => $post) {
          //Check if WPML is activated
          if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
            global $sitepress;

            $post_lang = $sitepress->get_language_for_element($post->ID, 'post_' . $type_post);
            $curr_lang = $sitepress->get_current_language();
            // Unset not translated posts
            if ( $post_lang != $curr_lang ) {
              unset( $latest[$key] );
            }
            // Post ID is different in a second language Solution
            if ( function_exists( 'icl_object_id' ) ) {
              $post = get_post( icl_object_id( $post->ID, $type_post, true ) );
            }
          }
          setup_postdata($post);
          $caption        = get_post_meta($post->ID, 'my_slider_caption', true);

          $output .= '<li class="list-item-'.$count.'">';
              $output .= '<duv class="excerpt">';
                $output .= stripslashes(htmlspecialchars_decode($caption));
              $output .= '</duv>';
          $output .= '</li>';
          $count++;
        }
        wp_reset_postdata(); // restore the global $post variable
        $output .= '</ul>';
      $output .= '</div>';
      return $output;
    }

    function bgSliderBuilder(){
      if ( is_front_page() ) {
    
        $rand_id = uniqid();
        // WPML filter
        $suppress_filters = get_option('suppress_filters');

        // Get Order & Orderby Parameters
        $orderby = ( of_get_option('slider_posts_orderby') ) ? of_get_option('slider_posts_orderby') : 'date';
        $order   = ( of_get_option('slider_posts_order') ) ? of_get_option('slider_posts_order') : 'DESC';

        // query
        $args = array(
          'post_type'        => 'slider',
          'posts_per_page'   => -1,
          'post_status'      => 'publish',
          'orderby'          => $orderby,
          'order'            => $order,
          'suppress_filters' => $suppress_filters
          );
        $slides = get_posts($args);
        if (empty($slides)) return;
      ?>

      <script type="text/javascript">
          jQuery(function() {
            jQuery('#full-bg-slider-<?php echo $rand_id ?>').cherryfullBgSlider({
              duration: 1000
            , parallaxEffect: false
            , animateLayout: 'simple-fade-eff'
            , prevButton: jQuery('.slider_prev')
            , nextButton: jQuery('.slider_next')
            });
          });
      </script>

      <?php
        $resutlOutput= '<div id="full-bg-slider-'.$rand_id.'" class="full-bg-slider">';
          $resutlOutput.= '<ul class="baseList">';
            foreach( $slides as $k => $slide ) {
              $url                = get_post_meta($slide->ID, 'my_slider_url', true);
              //$sl_image_url       = wp_get_attachment_image_src( get_post_thumbnail_id($slide->ID), 'slider-post-thumbnail');
              $sl_image_url       = wp_get_attachment_image_src( get_post_thumbnail_id($slide->ID), 'full');

              if ( $sl_image_url[0]=='' ) {
                $sl_image_url[0] = PARENT_URL."/images/blank.gif";
              }
              if ( $url!='' ) {
                $url = "data-link='$url'";
              }
              $resutlOutput.= '<li data-preview="'. $sl_image_url[0] .'" data-img-width="'. $sl_image_url[1] .'" data-img-height="'. $sl_image_url[2] .'"></li>';
              
            }
          $resutlOutput.= '</ul>';
        $resutlOutput.= '</div>';

        echo $resutlOutput;
        wp_reset_postdata();
      }

  }

}


new cherry_bgslider();
?>