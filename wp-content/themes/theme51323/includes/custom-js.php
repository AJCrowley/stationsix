<?php
add_action( 'wp_enqueue_scripts', 'cherry_child_custom_scripts' );

function cherry_child_custom_scripts() {
	/**
	 * How to enqueue script?
	 *
	 * @link http://codex.wordpress.org/Function_Reference/wp_enqueue_script
	 */
	wp_deregister_script('googlemapapis');
	wp_enqueue_script('googlemapapis', '//maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBharH6VcAt9dg0ZJYqfHGuClPcnIOcG_4&sensor=false', array('jquery'), false, false);
} ?>