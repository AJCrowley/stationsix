<?php
/**
 * Returns an array of system fonts
 * Feel free to edit this, update the font fallbacks, etc.
 */
function options_typography_get_os_extra_fonts() {
	// OS Font Defaults
	$os_faces = array(
		'"Kabel Bk BT", sans-serif'                            => 'Kabel Bk BT'
	);
	return $os_faces;
}
?>