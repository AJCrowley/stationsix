<?php /* Wrapper Name: Header */ ?>

<?php if( is_front_page() ) { ?>
	<a href="javascript:void(o)" class="slider_prev">prev</a>
	<a href="javascript:void(o)" class="slider_next">next</a>
<?php } ?>

<div class="row">
	<div class="span12 hidden-phone" data-motopress-type="static" data-motopress-static-file="static/static-search.php">
		<?php get_template_part("static/static-search"); ?>
	</div>
</div>

<div class="row">
	<div class="span12" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
		<?php get_template_part("static/static-logo"); ?>
	</div>
</div>

<div class="row">
	<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
		<?php get_template_part("static/static-nav"); ?>
	</div>
</div>