<?php /* Wrapper Name: Footer */ ?>

<?php if( is_front_page() ) { ?>
	<div class="custom_map">
		<div data-motopress-type="static" data-motopress-static-file="static/static-map.php">
			<?php get_template_part("static/static-map"); ?>
		</div>
	</div>
<?php } ?>

<div class="row">
	<div class="span12" data-motopress-type="static" data-motopress-static-file="static/static-footer-text.php">
		<?php get_template_part("static/static-footer-text"); ?>
	</div>
</div>

<div class="row">
	<!-- Social Links -->
	<div class="span12 social-nets-wrapper" data-motopress-type="static" data-motopress-static-file="static/static-social-networks.php">
		<?php get_template_part("static/static-social-networks"); ?>
	</div>
	<!-- /Social Links -->
</div>

<div class="row">
	<div class="span12" data-motopress-type="static" data-motopress-static-file="static/static-footer-nav.php">
		<?php get_template_part("static/static-footer-nav"); ?>
	</div>
</div>