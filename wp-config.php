<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stationsix');

/** MySQL database username */
define('DB_USER', 'stationsix');

/** MySQL database password */
define('DB_PASSWORD', 'dYmszBQaTCwaSERH');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>$}apxO7(smNYy[njYUYs=B}Ql3A[*amt<>$S0O-0ALe[6xRn,&!C?,u~2u[7m i');
define('SECURE_AUTH_KEY',  ';L`zge@< qFIl$6A6w}Y<~8m2%R[m?J`Xgy$k$,$3Bq}`!M/],[.K(!QV|T[C!k#');
define('LOGGED_IN_KEY',    '5fM<R<H,wq<qNJ-=7xR2d8]&BjA0ZRpYsUIi7ND6[U&+.9q`nE_ wASAHG8oo+,`');
define('NONCE_KEY',        '8HAIu<B+DJGhsp4vk8F9I!L2M}SNp HOghs`FR`P]e-Dni! p@6sfy!~jgnL)CCm');
define('AUTH_SALT',        'cVS%M5rI{5{g/ 5#`*AIn5_<f}KWs5kE3lnc#O&9CXa9mWJ-n<:= H_$B~8(N1=P');
define('SECURE_AUTH_SALT', '@,wOTU~/Nd|p-s[sGd$hA9{*<~HgeT-rMMIG;i4^/qg=LiQpIc]I5I`,q6bsg5vy');
define('LOGGED_IN_SALT',   'Z!V9+4v?)W1oP(U``)l};8Q:c|wjtn<dl6bCR}y#D!I$&0~;-TW013z5KYrB2t9l');
define('NONCE_SALT',       '$5&tw3e{S$HWpvX_,jL7FW3o&j_z3{irwinM&-kq9_sOBtXhpV+CT(%T[{#k!`Z%');

/**#@-*/
define('FS_METHOD', 'direct');
define('FS_CHMOD_DIR', (0775 & ~ umask()));
define('FS_CHMOD_FILE', (0664 & ~ umask()));

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 's6_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

